<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ne".
 *
 * @property int $idne
 * @property int $ide
 * @property int $idn
 *
 * @property Etiqueta $e
 * @property Noticia $n
 */
class Ne extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ne';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ide', 'idn'], 'integer'],
            [['ide'], 'exist', 'skipOnError' => true, 'targetClass' => Etiqueta::className(), 'targetAttribute' => ['ide' => 'ide']],
            [['idn'], 'exist', 'skipOnError' => true, 'targetClass' => Noticia::className(), 'targetAttribute' => ['idn' => 'idn']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idne' => 'Idne',
            'ide' => 'Ide',
            'idn' => 'Idn',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getE()
    {
        return $this->hasOne(Etiqueta::className(), ['ide' => 'ide']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getN()
    {
        return $this->hasOne(Noticia::className(), ['idn' => 'idn']);
    }
}
