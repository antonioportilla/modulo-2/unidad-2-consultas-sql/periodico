<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "picture".
 *
 * @property int $idp
 * @property string $foto
 * @property int $idf
 * @property int $idn
 *
 * @property Fotografo $f
 * @property Noticia $n
 */
class Picture extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'picture';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idf', 'idn'], 'integer'],
            [['foto'], 'string', 'max' => 255],
            [['idf'], 'exist', 'skipOnError' => true, 'targetClass' => Fotografo::className(), 'targetAttribute' => ['idf' => 'idf']],
            [['idn'], 'exist', 'skipOnError' => true, 'targetClass' => Noticia::className(), 'targetAttribute' => ['idn' => 'idn']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idp' => 'Idp',
            'foto' => 'Foto',
            'idf' => 'Idf',
            'idn' => 'Idn',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getF()
    {
        return $this->hasOne(Fotografo::className(), ['idf' => 'idf']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getN()
    {
        return $this->hasOne(Noticia::className(), ['idn' => 'idn']);
    }
}
