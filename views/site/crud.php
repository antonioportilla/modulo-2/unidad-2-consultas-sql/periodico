<?php

use yii\helpers\Html;

echo Html::a('Noticias',
        ['noticia/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Imagenes',
        ['picture/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Autores',
        ['autor/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Etiquetas',
        ['etiqueta/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Noticia_Etiqueta',
        ['ne/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Fotografos',
        ['fotografo/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Emails Fotografos',
        ['email/index'],
        ['class' => 'btn btn-primary btn-large']
        );

        


?>