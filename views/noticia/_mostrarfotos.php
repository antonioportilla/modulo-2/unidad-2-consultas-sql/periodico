<?php
use yii\helpers\Html;

?>
<div class="col-sm-4 col-md-6">
    <div class="thumbnail">
      <div class="caption">
        <p>
        <?=Html::img('@web/imgs/' . $model->foto, ['class' => 'img-thumbnail']);?>
        </p>
    </div>
  </div>
