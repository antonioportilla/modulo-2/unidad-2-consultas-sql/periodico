<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
<div class="col-sm-4 col-md-6">
    <div class="thumbnail">
        <div class="bg-gris">
        <?php
        //$etiquetas=ArrayHelper::getColumn($model->nes,"nombre");
        $etiquetas=$model->nes;
        
        foreach($etiquetas as $etiqueta){
            echo Html::a($etiqueta->nombre, ['noticia/fotosnoticia', 'id'=>$etiqueta->ide], ['class' => 'btn btn-white']);
        } 
        
        ?>
            </div>
      <div class="caption">
        <p>
          <h2><?= $model->titulo ?> </h2>
          
        </p>
        <p><?= $model->texto ?> </p>
        
        
        <p>
           <?= Html::a('Ver fotos', ['noticia/fotosnoticia', 'id'=>$model->idn], ['class' => 'btn btn-primary']);?>
           <?= Html::a('Informacion autor', ['noticia/autoresnoticia','id'=>$model->idn], ['class' => 'btn btn-primary']);?>
    </p>
                
    </div>
  </div>
