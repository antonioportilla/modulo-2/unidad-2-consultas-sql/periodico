<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<div class="col-sm-4 col-md-6">
    <div class="thumbnail">
      <div class="caption">
        <h2><?= $model->idf.' ' ?><?= $model->nombre ?> </h2>
        
        <p >
            <?= Html::a('PortFolio', 'https://www.pinterest.es/', ['class' => 'btn btn-primary'], ['target' => '_blank']);?>
           <?= Html::a('Fotos', ['fotografo/consultarpictures','id'=>$model->idf], ['class' => 'btn btn-primary']);?>
        </p>
        <p >
            <?= Html::a('Noticias', ['fotografo/consultarnoticias', 'id'=>$model->idf ], ['class' => 'btn btn-primary']);?>
            <?= Html::a('Autores', ['fotografo/consultarautores', 'id'=>$model->idf ], ['class' => 'btn btn-primary']);?>
            
        </p>
        <p>
            <?php 
            $correos=ArrayHelper::getColumn($model->emails, "email");
            $correos= implode(" , ", $correos);
            ?> 
            
            <?= $correos ?>
        </p>
    </div>
  </div>
