<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fotografo */

$this->title = 'Update Fotografo: ' . $model->idf;
$this->params['breadcrumbs'][] = ['label' => 'Fotografos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idf, 'url' => ['view', 'id' => $model->idf]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fotografo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
