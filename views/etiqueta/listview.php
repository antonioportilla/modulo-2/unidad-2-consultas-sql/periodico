<div>
<h1>Listado de noticias</h1>
</div>

<div class="row">
<?php
use yii\widgets\ListView;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar', /*Recomendado _ el guion  que se muestra una vista dentro de otra */
]);
?>
</div>