<?php
use yii\helpers\Html;
?>
<div class="col-sm-4 col-md-6">
    <div class="thumbnail">
      <div class="caption">
          
          <h1><?=$model->ida.', ' ?><?= $model->nombre ?></h1>
      
        <p><?= $model->email ?> </p>
        <p><?= $model->alias ?> </p>
          <p>
             <?php  echo Html::a('Ver Noticias', ['autor/consultarnoticias','id'=>$model->ida], ['class' => 'btn btn-primary']);  ?> 
           
        </p>  
                
    </div>
  </div>
