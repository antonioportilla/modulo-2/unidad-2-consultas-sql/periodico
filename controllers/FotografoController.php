<?php

namespace app\controllers;

use Yii;
use app\models\Fotografo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FotografoController implements the CRUD actions for Fotografo model.
 */
class FotografoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    
    
        public function actionConsultaemail($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fotografo::findone($id)->getFotografos()
            ]);
        return $this->render('listaemails', [
            'dataProvider' => $dataProvider,
            'autor'=> Fotografo::findone($id)->nombre,
        ]);
    }
    
    
    
    
    
    
    /**
     * Lists all Fotografo models.
     * @return mixed
     */
    public function actionList()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fotografo::find(),
            
             'pagination' => [
        'pageSize' => 4,]
        ]);

        return $this->render('listview', [
            'dataProvider' => $dataProvider,
            
        ]);
    }

    /**
     * Lists all Fotografo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fotografo::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Fotografo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fotografo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Fotografo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idf]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Fotografo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idf]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Fotografo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fotografo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fotografo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fotografo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionConsultarpictures($id){
        $dataProvider = new ActiveDataProvider([
            'query' => Fotografo::findone($id)->getPictures(),
            'pagination' => [
        'pageSize' => 4,
    ],
            
            ]);
        return $this->render('listapictures', [
            'dataProvider' => $dataProvider,
            'fotografo'=> Fotografo::findone($id)->nombre,
        ]);
               
        
        
    }
    
    
    /*
     * 
     * lista las noticias del fotografo
     */
        public function actionConsultarnoticias($id){
        $dataProvider = new ActiveDataProvider([
            'query' => Fotografo::findone($id)->getNoticias(),
            'pagination' => [
        'pageSize' => 4,
    ],
            
            ]);
        return $this->render('listarnoticias', [
            'dataProvider' => $dataProvider,
            'fotografo'=> Fotografo::findone($id)->nombre,
        ]);
               
        
        
    }
    
     /*
     * 
     * lista las noticias Autores
     */
        public function actionConsultarautores($id){
        $dataProvider = new ActiveDataProvider([
            'query' => Fotografo::findone($id)->getAutores(),
            'pagination' => [
        'pageSize' => 4,
    ],
            
            ]);
        return $this->render('listarautores', [
            'dataProvider' => $dataProvider,
            'fotografo'=> Fotografo::findone($id)->nombre,
        ]);
               
        
        
    }

    

}
